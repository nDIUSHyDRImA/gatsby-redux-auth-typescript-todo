import React from "react"
import { Provider } from "react-redux"
import { store } from "../app/store"
import Navbar from "../components/Navbar/Navbar"
import AppRouter from "../app/router/index"

export default function App() {
  return (
    <Provider store={store}>
      <Navbar />
      <AppRouter />
    </Provider>
  )
}
