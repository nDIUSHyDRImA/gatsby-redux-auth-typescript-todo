import React, { useState } from "react"
import { isLoggedIn, isCorrectUser } from "../../app/auth/auth"
import { useDispatch, useSelector } from "react-redux"
import { selectJwt } from "../../app/auth/slice"
import { navigate } from "gatsby"
import { login } from "../../app/auth/slice"

export default function Login() {
  const [username, setUsername] = useState("foo")
  const [password, setPassword] = useState("bar")
  const jwt = useSelector(selectJwt)
  const dispatch = useDispatch()

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    if (!isCorrectUser(username, password)) {
      alert("Invalid!")
      return
    }
    dispatch(login())
  }

  if (isLoggedIn(jwt)) {
    navigate("/app")
  }

  return (
    <form onSubmit={e => handleSubmit(e)}>
      <input
        type="text"
        className="input"
        value={username}
        onChange={e => setUsername(e.currentTarget.value)}
      />
      <input
        type="password"
        className="input"
        value={password}
        onChange={e => setPassword(e.currentTarget.value)}
      />
      <button type="submit" className="button is-primary">
        Login
      </button>
    </form>
  )
}
