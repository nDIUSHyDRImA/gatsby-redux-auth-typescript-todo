import { createSlice, PayloadAction } from "@reduxjs/toolkit"
import crypto from "crypto"
import { RootState } from "../../app/store"

type OneTodo = {
  key: string
  content: string
  isCompleted: boolean
}

type TodoState = {
  allTodo: OneTodo[]
}

const initialState: TodoState = {
  allTodo: [],
}

export const slice = createSlice({
  name: "todo",
  initialState,
  reducers: {
    add: (state, action: PayloadAction<string>) => {
      if (!action.payload) {
        return
      }

      const N = 16
      const key = crypto
        .randomBytes(N)
        .toString("base64")
        .substring(0, N)
      state.allTodo.push({
        key,
        content: action.payload,
        isCompleted: false,
      })
    },
    remove: (state, action: PayloadAction<string>) => {
      state.allTodo = state.allTodo.filter(todo => todo.key !== action.payload)
    },
    toggleComplete: (state, action: PayloadAction<string>) => {
      state.allTodo
        .filter(todo => todo.key === action.payload)
        .forEach(todo => (todo.isCompleted = !todo.isCompleted))
    },
  },
})

export const { add, remove, toggleComplete } = slice.actions

export const selectAllTodo = (state: RootState) => state.todo.allTodo

export default slice.reducer
