import React, { useState } from "react"
import { add } from "./slice"
import { useDispatch } from "react-redux"

export default function NewTodoForm() {
  const [newTodo, setNewTodo] = useState("")
  const dispatch = useDispatch()

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    dispatch(add(newTodo))
    setNewTodo("")
  }

  return (
    <form onSubmit={e => handleSubmit(e)}>
      <div className="field has-addons">
        <div className="control">
          <input
            type="text"
            className="input"
            placeholder="New Todo Here"
            value={newTodo}
            onChange={e => setNewTodo(e.currentTarget.value)}
          />
        </div>
        <div className="control">
          <button type="submit" className="button is-primary">
            Add
          </button>
        </div>
      </div>
    </form>
  )
}
