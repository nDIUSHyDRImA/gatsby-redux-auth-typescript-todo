import React from "react"
import NewTodoForm from "./NewTodoForm"
import AllTodo from "./AllTodo"

export default function Todo() {
  return (
    <>
      <NewTodoForm />
      <AllTodo />
    </>
  )
}
