import React from "react"
import { useSelector, useDispatch } from "react-redux"
import { selectAllTodo, remove, toggleComplete } from "./slice"
import styles from "./Todo.module.css"

export default function AllTodo() {
  const allTodo = useSelector(selectAllTodo)
  const dispatch = useDispatch()

  return (
    <ul>
      {allTodo.map(todo => (
        <li key={todo.key}>
          <label className="checkbox">
            <input
              type="checkbox"
              checked={todo.isCompleted}
              onChange={() => dispatch(toggleComplete(todo.key))}
            />
            <span className={todo.isCompleted ? styles.complete : ""}>
              {todo.content}
            </span>
          </label>
          <button
            type="button"
            className="button"
            onClick={() => dispatch(remove(todo.key))}
          >
            Remove
          </button>
        </li>
      ))}
    </ul>
  )
}
