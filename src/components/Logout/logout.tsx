import React, { useEffect } from "react"
import { navigate } from "gatsby"
import { useDispatch } from "react-redux"
import { logout } from "../../app/auth/slice"

export default function Logout() {
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(logout())
  })

  navigate("/app")

  return <div></div>
}
