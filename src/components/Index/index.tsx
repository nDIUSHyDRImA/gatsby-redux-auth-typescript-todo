import React from "react"
import { isLoggedIn } from "../../app/auth/auth"
import { selectJwt } from "../../app/auth/slice"
import { useSelector } from "react-redux"
import { Link } from "@reach/router"

export default function Index() {
  const jwt = useSelector(selectJwt)

  return (
    <>
      <p>Hyper Todo App</p>
      {isLoggedIn(jwt) ? (
        <Link to="/app/todo">todo</Link>
      ) : (
        <Link to="/app/login">login</Link>
      )}
    </>
  )
}
