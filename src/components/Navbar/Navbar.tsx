import React, { useState, useEffect } from "react"
import { Link } from "@reach/router"
import useResizeObserver from "use-resize-observer"
import styles from "./Navbar.module.css"
import { isLoggedIn } from "../../app/auth/auth"
import { selectJwt } from "../../app/auth/slice"
import { useSelector } from "react-redux"

export default function Navbar() {
  const [isActive, setIsActive] = useState(false)
  const { ref, width = 1 } = useResizeObserver()
  const [isFixedTop, setIsFixedTop] = useState(true)
  const htmlTagClassAttr = document.getElementsByTagName("html")[0].classList

  const jwt = useSelector(selectJwt)

  useEffect(() => {
    if (width < 1007) {
      setIsFixedTop(false)
      return
    }
    setIsFixedTop(true)
  }, [width])

  useEffect(() => {
    if (isFixedTop) {
      htmlTagClassAttr.remove("has-navbar-fixed-bottom")
      htmlTagClassAttr.add("has-navbar-fixed-top")
      return
    }
    htmlTagClassAttr.remove("has-navbar-fixed-top")
    htmlTagClassAttr.add("has-navbar-fixed-bottom")
  })

  return (
    <>
      <div
        role="button"
        className={isActive ? styles.navbarBackground : ""}
        onClick={() => setIsActive(false)}
        onKeyDown={() => setIsActive(false)}
        tabIndex={-1}
      ></div>
      <nav
        className={`navbar is-primary ${
          isFixedTop ? "is-fixed-top" : "is-fixed-bottom"
        }`}
        role="navigation"
        aria-label="main navigation"
        ref={ref}
      >
        <div className="navbar-brand">
          <Link
            className="navbar-item"
            to="/app"
            onClick={() => setIsActive(false)}
          >
            Gatsby Todo
          </Link>

          <span
            role="button"
            className={`navbar-burger burger ${isActive && "is-active"}`}
            aria-label="menu"
            aria-expanded="false"
            data-target="navbarBasicExample"
            onClick={() => setIsActive(!isActive)}
            onKeyDown={() => setIsActive(false)}
            tabIndex={-1}
          >
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
          </span>
        </div>

        <div
          id="navbarBasicExample"
          className={`navbar-menu ${isActive && "is-active"}`}
        >
          <div className="navbar-start"></div>

          <div className="navbar-end" role="button">
            {isLoggedIn(jwt) ? (
              <Link
                to="/app/logout"
                className="navbar-item"
                onClick={() => setIsActive(false)}
              >
                Logout
              </Link>
            ) : (
              <Link
                to="/app/login"
                className="navbar-item"
                onClick={() => setIsActive(false)}
              >
                Login
              </Link>
            )}
          </div>
        </div>
      </nav>
    </>
  )
}
