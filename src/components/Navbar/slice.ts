import { createSlice } from "@reduxjs/toolkit"
import { RootState } from "../../app/store"

type navbarState = {
  isActive: boolean
}

const initialState: navbarState = {
  isActive: false,
}

export const slice = createSlice({
  name: "navbar",
  initialState,
  reducers: {
    toggleIsActive: state => {
      state.isActive = !state.isActive
    },
    resetIsActive: state => {
      state.isActive = false
    },
  },
})

export const { toggleIsActive, resetIsActive } = slice.actions

export const selectIsActive = (state: RootState) => state.navbar.isActive

export default slice.reducer
