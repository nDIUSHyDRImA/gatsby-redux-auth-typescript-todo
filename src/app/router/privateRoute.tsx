import React from "react"
import { navigate } from "gatsby"
import { isLoggedIn } from "../auth/auth"
import { useSelector } from "react-redux"
import { selectJwt } from "../auth/slice"

const PrivateRoute = ({ component: Component, ...rest }) => {
  const jwt = useSelector(selectJwt)

  if (!isLoggedIn(jwt) && window.location.pathname !== `/app/login`) {
    navigate("/app/login")
    return null
  }

  return <Component {...rest} />
}

export default PrivateRoute
