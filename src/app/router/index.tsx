import React from "react"
import Index from "../../components/Index/index"
import Login from "../../components/Login/login"
import Todo from "../../components/Todo/todo"
import Logout from "../../components/Logout/logout"
import { Router } from "@reach/router"
import PrivateRoute from "./privateRoute"

export default function AppRouter() {
  return (
    <Router>
      <PublicRoute path="/app" component={Index} />
      <PublicRoute path="/app/login" component={Login} />
      <PrivateRoute path="/app/todo" component={Todo} />
      <PublicRoute path="/app/logout" component={Logout} />
    </Router>
  )
}

const PublicRoute = ({ component: Component, ...rest }) => (
  <Component {...rest} />
)
