import { configureStore, Action, ThunkAction } from "@reduxjs/toolkit"
import NavbarReducer from "../components/Navbar/slice"
import TodoReducer from "../components/Todo/slice"
import AuthReducer from "./auth/slice"

export const store = configureStore({
  reducer: {
    navbar: NavbarReducer,
    todo: TodoReducer,
    auth: AuthReducer,
  },
})

export type RootState = ReturnType<typeof store.getState>
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>
