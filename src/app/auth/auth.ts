const correctUsername = "foo"
const correctPassword = "bar"

export const isLoggedIn = (jwt: string) => !!jwt

export const isCorrectUser = (username: string, password: string) =>
  username === correctUsername && password === correctPassword
