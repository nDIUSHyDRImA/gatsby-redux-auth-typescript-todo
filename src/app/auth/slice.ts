import { createSlice } from "@reduxjs/toolkit"
import { RootState } from "../store"
import crypto from "crypto"

type authState = {
  jwt: string
}

const initialState: authState = {
  jwt: (localStorage != null && localStorage.getItem("jwt")) || "",
}

const slice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    login: state => {
      const N = 16
      const jwt = crypto
        .randomBytes(N)
        .toString("base64")
        .substring(0, N)
      localStorage.setItem("jwt", jwt)
      state.jwt = jwt
    },
    logout: state => {
      localStorage.removeItem("jwt")
      state.jwt = ""
    },
  },
})

export const { login, logout } = slice.actions

export const selectJwt = (state: RootState) => state.auth.jwt

export default slice.reducer
